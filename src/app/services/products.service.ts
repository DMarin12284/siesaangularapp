import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Injectable()
export class ProductsService {
    API_ENDPOINT = '/api/productos';
    private products:any = [];
   


    constructor(private httpclient: HttpClient ){
        
    }
    
    getProducts(){
      this.httpclient.get(this.API_ENDPOINT).subscribe((data)=>{
        this.products =  data;
      });
        return this.products;

    }

    getProductsId(idx: string){
      let id= parseInt(idx);
      
        return this.products[id];

    }
    
    getProduct(idx: string){
        let id= parseInt(idx);
      
        return this.products[id];
    }

    searchProduct(termino:string){

    let productsArr:Producto[] = [];
    termino = termino.toLowerCase();

    for( let i = 0; i < this.products.length; i ++ ){

      let product = this.products[i];

      let producto = product.producto.toLowerCase();

      if( producto.indexOf( termino ) >= 0  ){
        product.idx = i;
        productsArr.push( product )
      }

    }

    return productsArr;

    }
   
    save(product:Producto){
      const headers =  new HttpHeaders({'Content-Type': 'application/json'});
      console.log(product);
      return this.httpclient.post(this.API_ENDPOINT, product, {headers: headers});
  }

  update(product:Producto){
    const headers =  new HttpHeaders({'Content-Type': 'application/json'});
    console.log(product);
    return this.httpclient.put(this.API_ENDPOINT+'/'+ product.id, product, {headers: headers});
}

}

export interface Producto {
    titulo: string;
    descripcion: string;
    imagen: string;
    precio: string;
    descuento: string;
    id: number;
}
