import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductsService } from  '../../services/products.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html'
})
export class ProductComponent {

  product:any ={};

  constructor(private activatedroute: ActivatedRoute,
    private _productsservice: ProductsService) {

      this.activatedroute.params.subscribe(params => {
        this.product = this._productsservice.getProduct(params['id']);
      })
   }


}
