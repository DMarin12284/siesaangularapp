import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProductsService, Producto } from '../../services/products.service';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {
  API_ENDPOINT = '/api/productos';
  product:any={
    titulo: null,
    descripcion: null,
    precio: null,
    descuento: null,
    imagen: null
  };
  constructor(private _productsService: ProductsService) {
    
   }

  ngOnInit() {
  }
  saveProduct(){
    this._productsService.save(this.product).subscribe(data =>{
      alert('El producto se guardo exitosamente');
    }, (error)=>{
      console.log(error);
      alert('Ocurrio un error al momento de guardar el producto');
    });
  }

}
