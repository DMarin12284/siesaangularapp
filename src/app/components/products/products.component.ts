import { Component, OnInit } from '@angular/core';
import { ProductsService, Producto } from '../../services/products.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styles: []
})
export class ProductsComponent implements OnInit {
  API_ENDPOINT = '/api/productos';
    products:any = [];

  constructor(private _productsService: ProductsService,private router:Router,
    private httpclient: HttpClient ) {
      httpclient.get(this.API_ENDPOINT).subscribe((data)=>{
        this.products =  data;
      });
   }

  ngOnInit() {
    
    this.products = this._productsService.getProducts();


  }


  seeProduct(idx:number){
    this.router.navigate(['/product',idx])
  }

  seeProductid(idx:string) {
    this.router.navigate(['/edit',idx])

  }
}
