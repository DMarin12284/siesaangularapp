import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProductsService, Producto } from '../../services/products.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  API_ENDPOINT = '/api/productos';
  product:any ={};

  constructor(private activatedroute: ActivatedRoute,
    private _productsservice: ProductsService) {

      this.activatedroute.params.subscribe(params => {
        this.product = this._productsservice.getProductsId(params['id']);
      })
      console.log(this.product);
   }

  ngOnInit() {
  }

  updateProduct(){
    this._productsservice.update(this.product).subscribe(data =>{
      alert('El producto se guardo exitosamente');
    }, (error)=>{
      console.log(error);
      alert('Ocurrio un error al momento de guardar el producto');
    });
  }

}
